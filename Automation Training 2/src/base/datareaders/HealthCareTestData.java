package base.datareaders;

import base.utilities.Utilities;

import java.util.Properties;

public class HealthCareTestData {
    public String Device;

    public String StartDate;
    public String StartTime;
    public String Duration;
    public String Urgency;
    public String Description;
    public String CustomerJobNo;
    public String Region;
    public String AddressType;
    public String Site;
    public String Address;
    public String DeliveryMethod;
    public String Category;
    public String ServiceAgreementItem;


    public HealthCareTestData(Properties prop){
        Utilities.getTestData(prop,this);
    }
}
