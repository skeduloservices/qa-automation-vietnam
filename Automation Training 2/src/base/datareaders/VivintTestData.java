package base.datareaders;

import base.utilities.Utilities;

import java.security.PublicKey;
import java.util.Properties;

public class VivintTestData {
    public String Device;

    public String First_Name;
    public String Last_Name;
    public String Email;
    public String Phone;
    public String Address;
    public String Location;
    public String Setter_Name;
    public String Setter_ID;
    public String Do_you_own_home_business;
    public String From_How_interested;

    public String Setter_Url;


    public VivintTestData(Properties prop){
        Utilities.getTestData(prop,this);
    }
}
