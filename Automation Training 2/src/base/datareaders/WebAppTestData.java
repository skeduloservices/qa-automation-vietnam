package base.datareaders;

import base.utilities.Utilities;

import java.util.Properties;

public class WebAppTestData {
    public String Device;
    public String JobType;
    public String Description;
    public String Contact;
    public String Address;
    public String Urgency;
    public String HourDuration;
    public String MinuteDuration;
    public String Region;
    public WebAppTestData(Properties prop){
        Utilities.getTestData(prop,this);
    }
}
