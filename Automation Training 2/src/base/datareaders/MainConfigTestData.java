package base.datareaders;
import base.utilities.Utilities;
import java.util.Properties;

public class MainConfigTestData {
    public String environment;
    public String sandboxURL;
    public String prodURL;
    public String browserType;
    public String webAppURL;
    public String username;
    public String password;
    public MainConfigTestData(Properties prop){
        Utilities.getTestData(prop,this);
    }
}
