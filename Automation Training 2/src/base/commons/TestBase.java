package base.commons;
import base.utilities.Utilities;
import base.datareaders.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.asserts.SoftAssert;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

public class TestBase {
    public static WebDriver webDriver;
    public static SoftAssert softAssert;
    private String device = "web";
    protected MainConfigTestData configData;
    protected WebAppTestData webAppTestData;
    protected HealthCareTestData healthCareTestData;
    protected VivintTestData VivintTestData;
    public String testDataPath;


    /**
     * To set up test
     * @param automatedProjects projects to be atuomated
     * @param method name of the test script
     */
    public void setUpTest(AutomatedProjects automatedProjects, Method method){
        softAssert = new SoftAssert();
        InputStream input = null;
        try {
            Properties prop = new Properties();
            String appURL;
            String driversPath = Utilities.getBasePath()+"/ExternalDrivers/";
            String projectName;
            switch(automatedProjects){
                case webapp:
                    projectName = "webapp";
                    break;
                case healthcare:
                    projectName = "healthcare_addon";
                    break;
                case Vivint:
                    projectName = "Vivint";
                    break;
                default:
                    projectName = "bbraun";
            }

            input = new FileInputStream(Utilities.getBasePath() +"/src/projects/" + projectName +"/configs/configs.properties");
            prop.load(input);
            configData = new MainConfigTestData(prop);
            switch (configData.environment){
                case "prod":
                    appURL = configData.prodURL;
                    break;
                case "webapp":
                    appURL = configData.webAppURL;
                    break;
                default:
                    appURL = configData.sandboxURL;
                    break;
            }

            switch (automatedProjects){
                case webapp:
                    initiateWebAppTest(method,projectName);
                    device = webAppTestData.Device;
                    break;
                case BBraun:
                    break;
                case healthcare:
                    initiateHealthCareAddon(method, projectName);
                    device = healthCareTestData.Device;
                    break;
                case Vivint:
                    initiateVivint(method, projectName);
                    device = VivintTestData.Device;
                    break;
                default:
                    break;
            }
            switch(device){
                case "android":
                    //androidDriver = initiateAndroidEnvironment();
                    break;
                default:
                    if(configData.browserType.equalsIgnoreCase("chrome")){
                        webDriver = initChromeDriver(appURL,driversPath);
                    }
                    else{
                        webDriver = initFirefoxDriver(appURL,driversPath);
                    }
                    break;

            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    private static WebDriver initChromeDriver(String appURL, String driverPath){
        System.out.println("Launching google chrome with new profile.. ");
        System.setProperty("webdriver.chrome.driver", driverPath
                + "chromedriver");
        ChromeOptions ops = new ChromeOptions();
        ops.addArguments("--disable-notifications");
        //System.setProperty("webdriver.chrome.webDriver", driverPath + "chromedriver.exe");
        webDriver = new ChromeDriver(ops);
        //webDriver.manage().window().maximize();
        webDriver.navigate().to(appURL);
        return webDriver;
    }

    private static WebDriver initFirefoxDriver(String appURL, String driverPath){
        System.out.println("Launching Firefox browser..");
        System.setProperty("webdriver.gecko.driver", driverPath
                + "geckodriver");
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
        webDriver.navigate().to(appURL);
        return webDriver;
    }

    /**
     * Setting up test Data for LWB project
     * @param method name of the method of the test script
     */
    private void initiateWebAppTest(Method method, String projectName){
        InputStream input = null;
        testDataPath = Utilities.getBasePath()+"/src/projects/"+projectName+"/TestData/" + method.getName()+".properties";
        try{
            Properties prop = new Properties();
            input = new FileInputStream(testDataPath);
            prop.load(input);
            webAppTestData = new WebAppTestData(prop);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initiateHealthCareAddon(Method method, String projectName){
        InputStream input = null;
        testDataPath = Utilities.getBasePath()+"/src/projects/"+projectName+"/TestData/" + method.getName()+".properties";
        try{
            Properties prop = new Properties();
            input = new FileInputStream(testDataPath);
            prop.load(input);
            healthCareTestData = new HealthCareTestData(prop);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void initiateVivint(Method method, String projectName){
        InputStream input = null;
        testDataPath = Utilities.getBasePath()+"/src/projects/"+projectName+"/TestData/" + method.getName()+".properties";
        try{
            Properties prop = new Properties();
            input = new FileInputStream(testDataPath);
            prop.load(input);
            VivintTestData = new VivintTestData(prop);
        }catch(Exception ex){
            ex.printStackTrace();
        }finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void cleanUpTest(){
        if(webDriver!=null){
            webDriver.quit();
        }

    }

}
