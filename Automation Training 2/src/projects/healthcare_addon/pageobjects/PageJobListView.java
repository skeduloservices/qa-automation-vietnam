package projects.healthcare_addon.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;


import java.util.List;

public class PageJobListView extends PageGeneral{
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public PageJobListView(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }



    public void GoToFirstJob()throws InterruptedException {
    {
        WebElement baseTable = driver.findElement(By.tagName("table"));
        List<WebElement> rows = baseTable.findElements(By.tagName("tr"));

        int row_count = rows.size();
        for (int row = row_count - 1; row >= 0; row--) {
            List<WebElement> cols = rows.get(row).findElements(By.tagName("th"));
            int col_count = cols.size();
            Thread.sleep(300);
            for (int col = 0; col < col_count; col++){
                String celtext = cols.get(col).getText();
                Thread.sleep(200);
                if(celtext.contains("JOB-"))
                {
                    cols.get(col).click();
                    Thread.sleep(1000);
                    row = 0;
                    break;
                }
            }

        }
    }}



    //define page element


    By pagejoblist_tbody = By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody");
    By pagejoblist_jobdetails = By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div/div/table/tbody/tr[0]/td[2]");
}
