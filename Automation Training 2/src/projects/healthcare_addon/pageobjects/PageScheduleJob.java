package projects.healthcare_addon.pageobjects;

import base.datareaders.HealthCareTestData;
import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class PageScheduleJob extends PageGeneral{
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public PageScheduleJob(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public PageScheduleJob_Conflict ScheduleJob_Click(HealthCareTestData testData) throws InterruptedException {
        {
            Integer i_time = 800;
            //it's iframe
            List<WebElement> iframe_list = driver.findElements(By.tagName("iframe"));
            int index = 0;

            for (int i = 0; i < iframe_list.size(); i++) {
                if (iframe_list.get(i).isDisplayed()) {
                    index = i;
                    break;
                }
            }
            Thread.sleep(i_time);

            driver.switchTo().frame(index);

        /*
        List<WebElement> list_startdate = driver.findElements(schedulejob_StartDate);
        int a1 = list_startdate.size(); //found 2

        for(WebElement we_startdate:list_startdate)
        {
            if(we_startdate.isDisplayed())
            {
                Utilities.waitForElement(driver, schedulejob_StartDate).clear();
                Utilities.waitForElement(driver, schedulejob_StartDate).sendKeys(testData.StartDate);
                break;
            }
        }
        */

            Thread.sleep(i_time);

            WebElement startdate = Utilities.waitForElement(driver, schedulejob_StartDate).findElement(By.tagName("input"));
            startdate.click();
            Thread.sleep(i_time);
            startdate.clear();
            startdate.sendKeys(testData.StartDate);
            Thread.sleep(i_time);


            WebElement starttime = Utilities.waitForElement(driver, schedulejob_StartTime).findElement(By.tagName("input"));
            starttime.click();
            Thread.sleep(i_time);
            starttime.sendKeys(testData.StartTime);
            Thread.sleep(i_time);


            Utilities.waitForElement(driver, schedulejob_Duration).click();
            WebElement duration = Utilities.waitForElement(driver, schedulejob_Duration).findElement(By.tagName("input"));
            duration.clear();
            duration.sendKeys(testData.Duration);
            Thread.sleep(i_time*2);


            Select urgency = new Select(Utilities.waitForElement(driver, schedulejob_Urgency));
            urgency.selectByVisibleText(testData.Urgency);
            Thread.sleep(i_time*2);


            WebElement description = Utilities.waitForElement(driver, schedulejob_Description);
            description.click();
            description.sendKeys(testData.Description);
            Thread.sleep(i_time*2);

            Utilities.waitForElement(driver , schedulejob_Region).click();


            List<WebElement> we_Region = driver.findElements(By.className("slds-lookup__menu"));

            //we_Region.sendKeys(testData.Region);

            for(WebElement we_reg: we_Region)
            {
                if (we_reg.isDisplayed())
                {
                    we_reg.click();
                    break;
                }
            }

            Thread.sleep(i_time);

            Utilities.waitForElement(driver, schedulejob_AddressType).click();
            Utilities.waitForElement(driver, schedulejob_AddressType).sendKeys(testData.AddressType);
            Thread.sleep(i_time);

            Utilities.waitForElement(driver, schedulejob_Site).click();
            List<WebElement> we_AddressType = driver.findElements(By.className("slds-lookup__menu"));

            for(WebElement addtype: we_AddressType)
            {
                Thread.sleep(i_time - 100);
                if (addtype.isDisplayed())
                {
                    addtype.click();
                    break;
                }
            }

            Thread.sleep(i_time * 3);

            //Utilities.waitForElement(driver, schedulejob_CustomerJobNo).click();
            //Utilities.waitForElement(driver, schedulejob_CustomerJobNo).sendKeys(testData.CustomerJobNo);
            //Thread.sleep(400);

            //Utilities.waitForElement(driver, schedulejob_DeliveryMethod).click();
            //Utilities.waitForElement(driver, schedulejob_DeliveryMethod).sendKeys(testData.DeliveryMethod);
            //Thread.sleep(400);

            Select category = new Select(Utilities.waitForElement(driver, schedulejob_Category));
            category.selectByVisibleText(testData.Category);
            Thread.sleep(i_time);

            //Utilities.waitForElement(driver, schedulejob_Category).click();
            //Thread.sleep(500);
            //Utilities.waitForElement(driver, schedulejob_Category).sendKeys(testData.Category);
            //Thread.sleep(500);


            //Utilities.waitForElement(driver, schedulejob_DeliveryMethod).click();
            //Utilities.waitForElement(driver, schedulejob_DeliveryMethod).sendKeys(testData.DeliveryMethod);
            //Thread.sleep(200);

            Utilities.waitForElement(driver, schedulejob_ServiceAgreementItem).click();
            Utilities.waitForElement(driver, schedulejob_ServiceAgreementItem).sendKeys(testData.ServiceAgreementItem);
            Thread.sleep(i_time);

            Utilities.waitForElement(driver, schedulejob_ServiceAgreementItem).click();
            Thread.sleep(i_time);

            List<WebElement> we_CreateJob = driver.findElements(By.className("slds-button"));
            int a1 = we_CreateJob.size();

            for (WebElement createjob_btn: we_CreateJob)
            {
                Thread.sleep(i_time - 300);

                if (createjob_btn.isDisplayed())
                {
                    if(createjob_btn.getText().contains("Create Job"))
                    {
                        createjob_btn.click();
                        break;
                    }
                }
            }

            //Utilities.waitForElement(driver, schedulejob_CreateJob).click();

            Thread.sleep(i_time);
            //back to normal page
            driver.switchTo().defaultContent();

            return  new PageScheduleJob_Conflict(driver, softAssert);
        }
    }

    //define page element

    //By schedulejob_StartDate = By.name("startDate");
    By schedulejob_StartDate    = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[1]/div[1]/div/div");
    By schedulejob_StartTime    = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[1]/div[2]/div/div");

    By schedulejob_Duration     = By.name("jobDuration");
    By schedulejob_Urgency      = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[1]/div[4]/div/div/select");
    //By schedulejob_Description = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[2]/div/div");
    By schedulejob_Description  = By.id("undefined");
    By schedulejob_Region       = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[2]/div[1]/div[1]/div[2]/input");
    By schedulejob_AddressType  = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[2]/div[2]/div/div/div/select");
    By schedulejob_Site         = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[2]/div[3]/div[1]/div[2]/input");

    By schedulejob_CustomerJobNo = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[2]/section/div[2]/additional-fields-section/div/div[1]/div/div/ng-include/div/div/input");

    //By schedulejob_Address = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[1]/section/div[2]/div/div[1]/div[3]/div/div[1]/div[1]/span/span[2]");
    By schedulejob_DeliveryMethod = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[3]/section/div[2]/div/div[2]/div/div/div[2]/div/div/select");
    By schedulejob_Category     = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[3]/section/div[2]/div/div[2]/div/div/div[3]/div/div/select");
    By schedulejob_ServiceAgreementItem = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[3]/section/div[2]/div/div[2]/div/div/div[4]/div/div/select");
    By schedulejob_CreateJob    = By.xpath("//*[@id=\"modalContentEl\"]/div/ul/li[3]/section/div[2]/div/div[2]/div/div/div[4]/div/div/select");
    
}
