package projects.healthcare_addon.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class PageScheduleJob_Confirm extends PageGeneral{
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public PageScheduleJob_Confirm(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public PageJobListView ScheduleJob_Click() throws InterruptedException {
        Thread.sleep(1000);

        //it's iframe
        List<WebElement> iframe_list = driver.findElements(By.tagName("iframe"));
        int index = 0;

        for (int i = 0; i < iframe_list.size(); i++) {
            if (iframe_list.get(i).isDisplayed()) {
                index = i;
                break;
            }
        }
        Thread.sleep(500);

        driver.switchTo().frame(index);

        /*
        List<WebElement> buttonList = driver.findElements(By.tagName("button"));
        for (WebElement button: buttonList)
        {
           if(button.getText().contains("Go to Job Details Page")){
               button.click();
               break;
           }
        }
        Thread.sleep(1000);
        */

        driver.switchTo().defaultContent();

        return new PageJobListView(driver, softAssert);
    }


    //define page element

    //By ScheduleJob_Confirm_GoToJobDetailsPage = By.xpath("//*[@id=\"j_id0:j_id1\"]/div/div[2]/div[1]/div/div[2]/div/button[1]");

}
