package projects.healthcare_addon.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import projects.healthcare_addon.pageobjects.*;

import java.lang.reflect.Method;

public class ST_HCPAO_001 extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.healthcare,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }
    @Test
    public void ST_HCPAO_001(){
        try {
            PageLogin p_login = new PageLogin(webDriver, softAssert);
            PageContact p_contact = new PageContact(webDriver, softAssert);
            PageScheduleJob p_schedulejob = new PageScheduleJob(webDriver, softAssert);
            PageScheduleJob_Confirm pageScheduleJob_confirm = new PageScheduleJob_Confirm(webDriver, softAssert);
            PageWebAppLogin p_webapp_login = new PageWebAppLogin(webDriver, softAssert);
            PageJobListView p_joblistview = new PageJobListView(webDriver, softAssert);
            PageScheduleJob_Conflict p_scheduleconflict = new PageScheduleJob_Conflict(webDriver, softAssert);

            p_login.logIn(configData.username, configData.password);
            webDriver.navigate().to("https://healthcareqa.lightning.force.com/lightning/r/Contact/0036A00000ShPLKQA3/view");
            p_contact.ScheduleJob_Click();
            p_schedulejob.ScheduleJob_Click(healthCareTestData);

            p_scheduleconflict.ScheduleConflict_Yes_Click();
            Thread.sleep(1000);
            //pageScheduleJob_confirm.ScheduleJob_Click();

            webDriver.navigate().to("https://healthcareqa.lightning.force.com/lightning/o/sked__Job__c/list?filterName=00B6A0000075R4OUAU");

            Thread.sleep(4000);
            //p_webapp_login.clickAlternativeLink();
            //p_webapp_login.clickProdBtn();

            p_joblistview.GoToFirstJob();
            Thread.sleep(1000);

            softAssert.assertAll();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
