package projects.webapp.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class PageGeneral {
    protected WebDriver driver;
    public SoftAssert softAssert;

    //init Login page
    public PageGeneral(WebDriver driver, SoftAssert softAssert){
        this.driver = driver;
        this.softAssert = softAssert;
        PageFactory.initElements(driver, this);
    }
}
