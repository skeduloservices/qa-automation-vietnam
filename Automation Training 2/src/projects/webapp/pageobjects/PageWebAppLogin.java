package projects.webapp.pageobjects;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class PageWebAppLogin {
    protected WebDriver driver;
    public SoftAssert softAssert;

    //init Login page
    public PageWebAppLogin(WebDriver driver, SoftAssert softAssert){
        this.driver = driver;
        this.softAssert = softAssert;
        PageFactory.initElements(driver, this);
    }

    public PageWebAppLogin clickAlternativeLink(){
        Utilities.waitForElement(driver, alternativeLoginLinkLocator).click();
        return this;
    }

    public PageWebAppLogin clickSandBoxBtn(){
        Utilities.waitForElement(driver, sandboxButtonLocator).click();
        return this;
    }

    public PageScheduling logIn(String username, String password){
        Utilities.waitForElement(driver, usernameLocator).sendKeys(username);
        Utilities.waitForElement(driver,pwdLocator).sendKeys(password);
        Utilities.waitForElement(driver,loginBtnLocator).click();
        return new PageScheduling(driver,softAssert);
    }

    //define page element
    By alternativeLoginLinkLocator = By.xpath("//*[@id=\"root\"]/div/div[2]/a");
    By productButtonLocator = By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/button[1]");
    By sandboxButtonLocator = By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/button[2]");
    By usernameLocator = By.id("username");
    By pwdLocator = By.id("password");
    By loginBtnLocator = By.id("Login");
}
