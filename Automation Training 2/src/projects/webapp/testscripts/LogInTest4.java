package projects.webapp.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import projects.webapp.pageobjects.PageJobDetails;
import projects.webapp.pageobjects.PageLoginSB;
import projects.webapp.pageobjects.PageScheduling;
import projects.webapp.pageobjects.PageWebAppLogin;

import java.lang.reflect.Method;

public class LogInTest4 extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.webapp,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }
    @Test
    public void LogInTest4(){
        try {
            PageLoginSB p_loginsb = new PageLoginSB(webDriver, softAssert);
            p_loginsb.logIn(configData.username, configData.password);
            webDriver.navigate().to("https://healthcareqa.lightning.force.com/lightning/r/Contact/0036A00000ShPLKQA3/view");
            softAssert.assertAll();

            //pageScheduling.enterJobType("Single Booking");

        }catch(Exception ex){
            ex.printStackTrace();
        }

    }
}
