package projects.Vivint.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import projects.Vivint.pageobjects.Page_Login;
import projects.Vivint.pageobjects.Page_Setter_Scheduling;

import java.lang.reflect.Method;

public class VIN_002_Dispatch_Appointment extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.Vivint,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }
    @Test
    public void VIN_001_Create_Appointment(){
        try {
            Page_Login p_login = new Page_Login(webDriver, softAssert);
            Page_Setter_Scheduling p_setter_scheduling = new Page_Setter_Scheduling(webDriver, softAssert);

            p_login.logIn(configData.username, configData.password);
            webDriver.navigate().to(VivintTestData.Setter_Url);

            p_setter_scheduling.Create_Appointment(webDriver, softAssert, VivintTestData);

            softAssert.assertAll();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
