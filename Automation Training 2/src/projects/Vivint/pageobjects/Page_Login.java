package projects.Vivint.pageobjects;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class Page_Login {
    protected WebDriver driver;
    public SoftAssert softAssert;


    //init Login page
    public Page_Login(WebDriver driver, SoftAssert softAssert){
        this.driver = driver;
        this.softAssert = softAssert;
        PageFactory.initElements(driver, this);
    }


    public Page_Setter_Scheduling logIn(String username, String password){
        Utilities.waitForElement(driver, usernameLocator).sendKeys(username);
        Utilities.waitForElement(driver,pwdLocator).sendKeys(password);

        Utilities.waitForElement(driver,loginBtnLocator).click();

        return new Page_Setter_Scheduling(driver,softAssert);
    }

    //define page element

    By usernameLocator = By.id("username");
    By pwdLocator = By.id("password");
    By loginBtnLocator = By.id("Login");
}
