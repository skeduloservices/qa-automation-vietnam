package projects.Vivint.pageobjects;

import base.datareaders.VivintTestData;
import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;
import sun.awt.windows.ThemeReader;
import java.util.List;

public class Page_Setter_Scheduling {
    protected WebDriver driver;
    public SoftAssert softAssert;
    private int i_time = 600;

    public Page_Setter_Scheduling(WebDriver driver, SoftAssert softAssert){
        this.driver = driver;
        this.softAssert = softAssert;
        PageFactory.initElements(driver, this);
    }



    public void Create_Appointment(WebDriver driver, SoftAssert softAssert, VivintTestData testdata) throws InterruptedException {
    {
        Utilities.waitForElement(driver, Get_Started_btn).click();
        Thread.sleep(i_time * 5);

        Utilities.waitForElement(driver, First_Name_txt).click();
        Utilities.waitForElement(driver, First_Name_txt).sendKeys(testdata.First_Name);

        Utilities.waitForElement(driver, Last_Name_txt).click();
        Utilities.waitForElement(driver, Last_Name_txt).sendKeys(testdata.Last_Name);

        Utilities.waitForElement(driver, Email_txt).click();
        Utilities.waitForElement(driver, Email_txt).sendKeys(testdata.Email);

        Utilities.waitForElement(driver, Phone_txt).click();
        Utilities.waitForElement(driver, Phone_txt).sendKeys(testdata.Phone);

        Utilities.waitForElement(driver, Address_txt).click();
        Thread.sleep(i_time);
        Utilities.waitForElement(driver, Address_input_txt).click();
        Utilities.waitForElement(driver, Address_input_txt).sendKeys(testdata.Address);
        Thread.sleep(i_time * 4);
        Utilities.waitForElement(driver, Address_result_btn).click();
        Thread.sleep(i_time * 2);

        Utilities.waitForElement(driver, Location_pkl).click();
        Thread.sleep(i_time * 4);
        Utilities.waitForElement(driver, Location_input_txt).click();
        Thread.sleep(i_time * 4);
        Utilities.waitForElement(driver, Location_input_txt).sendKeys(testdata.Location);
        Thread.sleep(i_time * 4);
        Utilities.waitForElement(driver, Location_result_btn).click();
        Thread.sleep(i_time * 4);

        Utilities.waitForElement(driver, Setter_Name).click();
        Utilities.waitForElement(driver, Setter_Name).sendKeys(testdata.Setter_Name);

        Utilities.waitForElement(driver, Setter_ID).click();
        Utilities.waitForElement(driver, Setter_ID).sendKeys(testdata.Setter_ID);

        Utilities.waitForElement(driver, Do_you_own_home).click();
        Utilities.waitForElement(driver, Do_you_own_home_Yes).click();
        Thread.sleep(i_time);

        List<WebElement> list_controls = driver.findElements(By.className("cube-popup"));
        int a1 = list_controls.size();
        for(WebElement we_cube_picker: list_controls)
        {
            if(we_cube_picker.isDisplayed()){
                we_cube_picker.findElement(By.className("cube-picker-confirm")).click();
                Thread.sleep(i_time);
                break;
            }
        }

        Utilities.waitForElement(driver, From_1_10).click();
        Utilities.waitForElement(driver, From_1_10_1).click();
        Thread.sleep(i_time);
        List<WebElement> list_from_1_10 = driver.findElements(By.className("cube-popup"));
        a1 = list_from_1_10.size();
        for(WebElement we_cube_picker: list_from_1_10)
        {
            if(we_cube_picker.isDisplayed()){
                we_cube_picker.findElement(By.className("cube-picker-confirm")).click();
                Thread.sleep(i_time);
                break;
            }
        }

    }}

    //define page element
    //By Get_Started_btn = By.xpath("/html/body/div[1]/div[2]/div/button");
    By Get_Started_btn = By.xpath("/html/body/div[1]/div/div/button");
    By First_Name_txt       = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/div/div[2]/div/input");
    By Last_Name_txt        = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div/input");
    By Email_txt            = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[3]/div/div[2]/div/input");
    By Phone_txt            = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[4]/div/div[2]/div/input");
    By Address_txt          = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[5]/div/div[2]/div/div/input");
    By Address_input_txt    = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[5]/div/div[2]/div/div[2]/div/header/div/div/input");
    By Address_result_btn   = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[1]/div[2]/div[5]/div/div[2]/div/div[2]/div/div/main/button");
    By Location_pkl         = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/div[2]/div/div/input");
    By Location_input_txt   = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/header/div/div/input");
    By Location_result_btn  = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/div/main/button[1]");
    By Setter_Name          = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[3]/div[1]/div/div[2]/div/input");
    By Setter_ID            = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[3]/div[2]/div/div[2]/div/input");
    By Do_you_own_home      = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[4]/div[1]/div/div[2]/div");
    By Do_you_own_home_Yes  = By.xpath("/html/body/div[3]/div[2]/div/div/div[2]/div/div/ul/li[2]");
    By Do_you_own_home_Done = By.xpath("/html/body/div[3]/div[2]/div/div/div[1]/span[2]");
    By From_1_10            = By.xpath("/html/body/div[1]/div[2]/div[2]/div[1]/div[2]/div[4]/div[2]/div/div[2]/div");
    By From_1_10_1          = By.xpath("/html/body/div[4]/div[2]/div/div/div[2]/div/div/ul/li[1]");
    By From_1_10_Done = By.xpath("/html/body/div[4]/div[2]/div/div/div[1]/span[2]");
}
