package projects.Vivint.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import base.page.JobDetails_Classic;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import projects.Vivint.pageobjects.Page_Login;
import projects.Vivint.pageobjects.Page_Setter_Scheduling;
import base.sked__object.sked__Job__c;


import java.lang.reflect.Method;

public class VIN_001_Create_Appointment extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.Vivint,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }
    @Test
    public void VIN_001_Create_Appointment(){
        try {
            Page_Login              p_login                 = new Page_Login(webDriver, softAssert);
            Page_Setter_Scheduling  p_setter_scheduling     = new Page_Setter_Scheduling(webDriver, softAssert);
            JobDetails_Classic      p_job_classic           = new JobDetails_Classic(webDriver, softAssert);
            sked__Job__c            myjob                   = new sked__Job__c();
            String job;

            job = "https://cs47.salesforce.com/a072a000000w2j3";

            p_login.logIn(configData.username, configData.password);
            //webDriver.navigate().to(VivintTestData.Setter_Url);

            webDriver.navigate().to(job);
           myjob =  p_job_classic.Get_Job_Info();

            //p_setter_scheduling.Create_Appointment(webDriver, softAssert, VivintTestData);

            softAssert.assertAll();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
