package projects.healthcare_addon.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import base.page.*;
import java.lang.reflect.Method;
import base.sked__object.sked__Job__c;

public class DRAFT_TD_001 extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.healthcare,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }

    public DRAFT_TD_001() {
    }

    @Test
    public void DRAFT_TD_001(){
        try {
            PageLogin                   p_login                 = new PageLogin(webDriver, softAssert);
            JobDetails_Lightning        p_jobdetails            = new JobDetails_Lightning(webDriver, softAssert);
            sked__Job__c                job                     = new sked__Job__c();
            String                      JOB_0191;

            JOB_0191 = "https://healthcareqa.lightning.force.com/lightning/r/sked__Job__c/a0E6A000008KBqdUAG/view";

            p_login.logIn(configData.username, configData.password);

            //Open job record
            webDriver.navigate().to(JOB_0191);
            Thread.sleep(6000);
            job = p_jobdetails.Get_Job_Info();

            softAssert.assertAll();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
