package projects.healthcare_addon.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import base.page.JobDetails_Lightning;
import base.page.JobDetails_webapp;
import base.page.PageLogin;
import base.page.PageWebAppLogin;
import base.sked__object.sked__Job__c;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;

public class DRAFT_TD_002 extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.healthcare,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }

    public DRAFT_TD_002() {
    }

    @Test
    public void DRAFT_TD_002(){
        try {
            PageLogin                   p_login                 = new PageLogin(webDriver, softAssert);
            PageWebAppLogin             p_wapp_login            = new PageWebAppLogin(webDriver, softAssert);
            JobDetails_webapp           p_jobdetails            = new JobDetails_webapp(webDriver, softAssert);
            sked__Job__c                job                     = new sked__Job__c();
            String                      job_record;

            p_wapp_login.AlternativeLink_Click();
            p_wapp_login.ProdBtn_Click();
            p_login.logIn(configData.username, configData.password);
            Thread.sleep(2000);

            job_record = "https://new.skedulo.com/jobdetails/a0E6A000008KCVMUA4?f.rt=p";

            //Open job record
            webDriver.navigate().to(job_record);
            Thread.sleep(2000);

            job = p_jobdetails.Get_Job_Info();

            softAssert.assertAll();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
