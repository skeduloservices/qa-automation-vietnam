package projects.healthcare_addon.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import base.utilities.Utilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import projects.healthcare_addon.pageobjects.*;
import base.sked__object.sked__Job__c;
import base.page.PageLogin;
import java.lang.reflect.Method;

public class ST_HCPAO_000 extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.healthcare,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }

    public ST_HCPAO_000() {
    }

    @Test
    public void ST_HCPAO_000(){
        try {
            PageLogin                   p_login                 = new PageLogin(webDriver, softAssert);
            PageContact                 p_contact               = new PageContact(webDriver, softAssert);
            PageScheduleJob             p_schedulejob           = new PageScheduleJob(webDriver, softAssert);
            PageScheduleJob_Confirm     pageScheduleJob_confirm = new PageScheduleJob_Confirm(webDriver, softAssert);
            //PageWebAppLogin             p_webapp_login          = new PageWebAppLogin(webDriver, softAssert);
            PageJobListView             p_joblistview           = new PageJobListView(webDriver, softAssert);
            PageScheduleJob_Conflict    p_scheduleconflict      = new PageScheduleJob_Conflict(webDriver, softAssert);
            PageJobDetails              p_jobdetails            = new PageJobDetails(webDriver, softAssert);
            sked__Job__c                job1                    = new sked__Job__c(healthCareTestData);
            sked__Job__c                job2                    = new sked__Job__c();


            p_login.logIn(configData.username, configData.password);

            //Open contact record
            webDriver.navigate().to("https://healthcareqa.lightning.force.com/lightning/r/Contact/0036A00000ShPLKQA3/view");
            p_contact.ScheduleJob_Click();
            p_schedulejob.ScheduleJob_Click(healthCareTestData);

            p_scheduleconflict.ScheduleConflict_Yes_Click();
            Thread.sleep(1000);
            //pageScheduleJob_confirm.ScheduleJob_Click();

            //Open Job View
            webDriver.navigate().to("https://healthcareqa.lightning.force.com/lightning/o/sked__Job__c/list?filterName=00B6A0000075R4OUAU");

            Thread.sleep(4000);
            //p_webapp_login.clickAlternativeLink();
            //p_webapp_login.clickProdBtn();

            p_joblistview.GoToFirstJob();
            Thread.sleep(1000);

            String temp;
            temp = Utilities.Get_Job_ID(p_jobdetails.Get_Job_ID());

            Thread.sleep(1000);

            softAssert.assertAll();

        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
