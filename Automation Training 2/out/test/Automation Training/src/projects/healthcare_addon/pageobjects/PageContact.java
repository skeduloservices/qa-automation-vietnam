package projects.healthcare_addon.pageobjects;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class PageContact extends PageGeneral{
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public PageContact(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public PageScheduleJob ScheduleJob_Click() throws InterruptedException {
        Thread.sleep(8000);
        List<WebElement> buttonList = driver.findElements(By.tagName("button"));
        for (WebElement button: buttonList
             ) {
           if(button.getText().contains("Schedule Job")){
               button.click();
               break;
           }
        }
        Thread.sleep(12000);
        return  new PageScheduleJob(driver, softAssert);
    }


    //define page element

    By scheduleJobBtnSelector = By.xpath("//*[@id=\"brandBand_1\"]/div/div[1]/div/div[1]/div/div[1]/div[2]/button[1]");



}
