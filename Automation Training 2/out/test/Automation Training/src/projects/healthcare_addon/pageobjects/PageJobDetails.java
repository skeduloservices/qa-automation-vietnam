package projects.healthcare_addon.pageobjects;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import java.util.List;

public class PageJobDetails extends PageGeneral{
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public PageJobDetails(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }



    public String Get_Job_ID()
    {
        String url;
        Utilities.waitForElement(driver, By.className("test-id__field-label-container"));
        url = driver.getCurrentUrl();

        return url;
    }



    //define page element


    By JobName = By.className("test-id__field-label-container");

}
