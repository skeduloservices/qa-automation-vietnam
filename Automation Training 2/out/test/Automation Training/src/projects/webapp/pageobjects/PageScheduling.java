package projects.webapp.pageobjects;

import base.datareaders.WebAppTestData;
import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

public class PageScheduling extends PageGeneral{

    public PageScheduling(WebDriver driver, SoftAssert softAssert){
        super(driver,softAssert);
    }

    public PageScheduling createJob(WebAppTestData testData){
        try {
            clickCreateNewBtn();
            Utilities.waitForElement(driver, createNewJobLocator).click();
            enterJobType(testData.JobType);
            enterDescription(testData.Description);
            enterContact(testData.Contact);
            enterAddress(testData.Address);
            enterUrgency(testData.Urgency);
            enterJobDuration(testData.HourDuration, testData.MinuteDuration);
            enterRegion(testData.Region);
            clickSaveandView();

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return this;
    }

    public PageScheduling clickCreateNewBtn(){
        Utilities.waitForElement(driver,createNewBtnLocator).click();
        return this;
    }

    public PageScheduling enterJobType(String data){
        WebElement we = Utilities.waitForElement(driver, jobTypeLocator).findElement(By.tagName("input"));
        we.sendKeys(data);
        we.sendKeys(Keys.ENTER);
        return this;
    }

    public PageScheduling enterDescription(String data){
        WebElement we = Utilities.waitForElement(driver, descriptionLocator).findElement(By.tagName("textarea"));
        we.sendKeys(data);
        return this;
    }

    public PageScheduling enterContact(String data) throws InterruptedException {
        WebElement we = Utilities.waitForElement(driver, contactLocator).findElement(By.tagName("input"));
        we.sendKeys(data);
        Thread.sleep(3000);
        we.sendKeys(Keys.ARROW_DOWN);
        we.sendKeys(Keys.ENTER);
        return this;
    }

    public PageScheduling enterAddress(String data) throws InterruptedException {
        WebElement we = Utilities.waitForElement(driver, addressLocator).findElement(By.tagName("input"));
        we.sendKeys(data);
        Thread.sleep(3000);
        we.sendKeys(Keys.ARROW_DOWN);
        we.sendKeys(Keys.ENTER);
        return this;
    }

    public PageScheduling enterUrgency(String data){
        WebElement we = Utilities.waitForElement(driver, urgencyLocator).findElement(By.tagName("input"));
        we.clear();
        we.sendKeys(data);
        we.sendKeys(Keys.ENTER);
        return this;
    }

    public PageScheduling enterJobDuration(String hour, String minute){
        WebElement we1 = Utilities.waitForElement(driver, jobDurationHourLocator).findElement(By.tagName("input"));
        WebElement we2 = Utilities.waitForElement(driver, jobDurationMinuteLocator).findElement(By.tagName("input"));
        we1.clear();
        we2.clear();
        we1.sendKeys(hour);
        we2.sendKeys(minute);
        return this;
    }

    public PageScheduling enterRegion(String data) throws InterruptedException {
        WebElement we = Utilities.waitForElement(driver, jobRegionLocator).findElement(By.tagName("input"));
        we.sendKeys(data);
        Thread.sleep(3000);
        we.sendKeys(Keys.ARROW_DOWN);
        we.sendKeys(Keys.ENTER);
        return this;
    }

    public PageJobDetails clickSaveandView(){
        Utilities.waitForElement(driver, saveDropdownLocator).click();
        Utilities.waitForElement(driver, saveandviewBtnLocator).click();
        return new PageJobDetails(driver, softAssert);
    }

    //Web Element
    By createNewBtnLocator = By.xpath("//*[@id=\"root\"]/div/div[1]/header/div/div/div[2]/ul/li[2]/div/button");
    By createNewJobLocator = By.id("linkCreateJob");

    //Job modal fields
    By jobTypeLocator = By.id("quickJobType");
    By descriptionLocator = By.id("quickJobDescription");
    By contactLocator = By.id("quickJobContact");
    By addressLocator = By.id("quickJobAddress");
    By urgencyLocator = By.id("quickJobUrgency");
    By jobDurationHourLocator = By.id("quickJobDurationHour");
    By jobDurationMinuteLocator = By.id("quickJobDurationMinute");
    By jobRegionLocator = By.id("createJobLocationRegion");
    By saveDropdownLocator = By.xpath("//*[@id=\"root\"]/div/div[3]/div/div/div[3]/div/div[1]/button");
    By saveandviewBtnLocator = By.id("buttonJobCreateSaveView");

}
