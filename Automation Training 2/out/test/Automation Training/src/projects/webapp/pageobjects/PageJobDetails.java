package projects.webapp.pageobjects;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

public class PageJobDetails extends PageGeneral {
    public PageJobDetails(WebDriver driver, SoftAssert softAssert){
        super(driver,softAssert);
    }

    public void verifyJobType(String data){
        String actual = Utilities.waitForElement(driver, jobTypeLocator).getText().replace("Job Type", "").replace("\n","");
        softAssert.assertEquals(actual, data, "The value does not match! Actual data: " + actual + ". Expected: " + data );
    }

    //Page field
    By jobTypeLocator = By.id("jobDetailsType");
    By jobDescLocator = By.id("jobDetailsDescription");
    By jobContactLocator = By.id("jobDetailsContact");
    By jobRegion = By.id("jobDetailsRegion");
    By jobAddressLocator = By.id("jobDetailsAddress");

}
