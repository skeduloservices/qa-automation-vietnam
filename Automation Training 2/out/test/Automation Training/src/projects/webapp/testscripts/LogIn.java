package projects.webapp.testscripts;

import base.commons.TestBase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import base.commons.AutomatedProjects;
import base.commons.TestEnvironments;
import base.utilities.Utilities;
import projects.webapp.pageobjects.PageJobDetails;
import projects.webapp.pageobjects.PageScheduling;
import projects.webapp.pageobjects.PageWebAppLogin;

import java.lang.reflect.Method;

public class LogIn extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.webapp,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }
    @Test
    public void LogIn(){
        try {
            PageWebAppLogin pageWebAppLogin = new PageWebAppLogin(webDriver, softAssert);
            PageJobDetails pageJobDetails = new PageJobDetails(webDriver, softAssert);
            PageScheduling pageScheduling = new PageScheduling(webDriver,softAssert);
            pageWebAppLogin.clickAlternativeLink();
            pageWebAppLogin.clickSandBoxBtn();
            pageWebAppLogin.logIn(configData.username,configData.password);
            pageScheduling.createJob(webAppTestData);
            pageJobDetails.verifyJobType(webAppTestData.JobType);
            softAssert.assertAll();

            //pageScheduling.enterJobType("Single Booking");

        }catch(Exception ex){
            ex.printStackTrace();
        }

    }
}
