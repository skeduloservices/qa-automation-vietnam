package projects.webapp.testscripts;

import base.commons.AutomatedProjects;
import base.commons.TestBase;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import projects.webapp.pageobjects.PageJobDetails;
import projects.webapp.pageobjects.PageScheduling;
import projects.webapp.pageobjects.PageWebAppLogin;

import java.lang.reflect.Method;

public class LogInTest3 extends TestBase {
    @BeforeMethod
    public void setUp(Method method){
        super.setUpTest(AutomatedProjects.webapp,method);
    }
    @AfterMethod
    public void tearDown(){
        super.cleanUpTest();
    }
    @Test
    public void LogInTest3(){
        try {
            PageWebAppLogin pageWebAppLogin = new PageWebAppLogin(webDriver, softAssert);
            PageJobDetails pageJobDetails = new PageJobDetails(webDriver, softAssert);
            PageScheduling pageScheduling = new PageScheduling(webDriver,softAssert);
            pageWebAppLogin.clickAlternativeLink();
            pageWebAppLogin.clickSandBoxBtn();
            pageWebAppLogin.logIn(configData.username,configData.password);
            pageScheduling.createJob(webAppTestData);
            pageJobDetails.verifyJobType(webAppTestData.JobType);
            softAssert.assertAll();

            //pageScheduling.enterJobType("Single Booking");

        }catch(Exception ex){
            ex.printStackTrace();
        }

    }
}
