package base.sked__object;

import base.datareaders.base_TestData;
import base.utilities.Utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

public class sked__Job__c extends sked__Object{

    //define job properties
    public  String sked__Start__c;
    public  String sked__Finish__c;
    public  String sked__Duration__c;
    public  String sked__Region__c;
    public  String sked__Timezone__c;
    public  String sked__Type__c	;
    public  String sked__Address__c;
    public  String sked__Job_Status__c;
    public  String sked__Urgency__c;

    //Init job data
    //TODO: Initiate job data

    public sked__Job__c(){}

    public sked__Job__c(base_TestData testData){
        this.sked__Region__c = testData.Region;
        //this.sked__Duration__c = testData.Duration;
        this.sked__Address__c = testData.Address;
        this.sked__Description__c = testData.Description;
    }


}
