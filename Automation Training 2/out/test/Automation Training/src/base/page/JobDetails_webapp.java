package base.page;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import base.sked__object.sked__Job__c;

public class JobDetails_webapp extends JobDetails_base {
    //protected WebDriver driver;
    //public SoftAssert softAssert;

    //init Login page
    public JobDetails_webapp(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public sked__Job__c Get_Job_Info()
    {
        String container_classname = "overflow-tooltip__text";
        sked__Job__c job = new sked__Job__c();

        WebElement we = Utilities.waitForElement(driver, JobType);
        job.sked__Type__c = we.findElement(By.className(container_classname)).getText();

        we = Utilities.waitForElement(driver, JobDescription);
        job.sked__Description__c = we.findElement(By.className(container_classname)).getText();

        we = Utilities.waitForElement(driver, JobAddress);
        job.sked__Address__c = we.findElement(By.className(container_classname)).getText();

        return job;
    }

    //define page element

    By JobType = By.id("jobDetailsType");
    By JobDescription = By.id("jobDetailsDescription");
    By JobAddress = By.id("jobDetailsAddress");


}
