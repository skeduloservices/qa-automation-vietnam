package base.page;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class PageLogin {
    protected WebDriver driver;
    public SoftAssert softAssert;


    //init Login page
    public PageLogin(WebDriver driver, SoftAssert softAssert){
        this.driver = driver;
        this.softAssert = softAssert;
        PageFactory.initElements(driver, this);
    }


    public void logIn(String username, String password){
        Utilities.waitForElement(driver, usernameLocator).sendKeys(username);
        Utilities.waitForElement(driver,pwdLocator).sendKeys(password);
        Utilities.waitForElement(driver,loginBtnLocator).click();
        //return new PageScheduling(driver,softAssert);
    }

    //define page element

    By usernameLocator = By.id("username");
    By pwdLocator = By.id("password");
    By loginBtnLocator = By.id("Login");
}
