package base.page;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

public class JobDetails_base extends general_page {
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public JobDetails_base(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public  String sked__Start__c;
    public  String sked__Finish__c;
    public  String sked__Duration__c;
    public  String sked__Region__c;
    public  String sked__Timezone__c;
    public  String sked__Type__c;
    public  String sked__Address__c;
    public  String sked__Description__c	;
    public  String sked__Job_Status__c;
    public  String sked__Urgency__c;


    //define page element






}
