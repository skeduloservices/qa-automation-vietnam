package base.page;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;


public class PageWebAppLogin {
    protected WebDriver driver;
    public SoftAssert softAssert;


    public PageWebAppLogin(WebDriver driver, SoftAssert softAssert) {
        this.driver = driver;
        this.softAssert = softAssert;
        PageFactory.initElements(driver, this);
    }

    public PageWebAppLogin AlternativeLink_Click() {
        Utilities.waitForElement(this.driver, this.alternativeLoginLinkLocator).click();
        return this;
    }

    public PageWebAppLogin SandBoxBtn_Click() {
        Utilities.waitForElement(this.driver, this.sandboxButtonLocator).click();
        return this;
    }

    public PageWebAppLogin ProdBtn_Click() {
        Utilities.waitForElement(this.driver, this.productButtonLocator).click();
        return this;
    }

    public PageLogin ProdBtn_Click_SF_Login() {
        Utilities.waitForElement(this.driver, this.productButtonLocator).click();
        return new PageLogin(driver, softAssert);
    }

    By alternativeLoginLinkLocator  = By.xpath("//*[@id=\"root\"]/div/div[2]/a");
    By productButtonLocator         = By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/button[1]");
    By sandboxButtonLocator         = By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/button[2]");
    By usernameLocator              = By.id("username");
    By pwdLocator                   = By.id("password");
    By loginBtnLocator              = By.id("Login");


}
