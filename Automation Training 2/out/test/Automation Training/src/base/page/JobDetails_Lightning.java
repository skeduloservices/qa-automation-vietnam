package base.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import java.util.List;
import base.sked__object.sked__Job__c;

public class JobDetails_Lightning extends JobDetails_base {
    //protected WebDriver driver;
    //public SoftAssert softAssert;

    //init Login page
    public JobDetails_Lightning(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public sked__Job__c Get_Job_Info() {

        sked__Job__c job = new sked__Job__c();
        String uiOutputTextArea = "uiOutputTextArea";

        List<WebElement> list_we = driver.findElements(By.className("slds-form-element_readonly"));

        //int i = list_we.size();

        for (WebElement we: list_we) {

            if(we.getText().contains("Description")){
                WebElement parent = we.findElement(By.xpath(".."));
                WebElement we_Description = parent.findElement(By.className(uiOutputTextArea));
                job.sked__Description__c = we_Description.getText();
            }


            if(we.getText().contains("Address")){
                WebElement parent = we.findElement(By.xpath(".."));
                WebElement we_Address = parent.findElement(By.className(uiOutputTextArea));
                job.sked__Address__c = we_Address.getText();
            }
            if(we.getText().contains("Timezone")){
                WebElement parent = we.findElement(By.xpath(".."));
                WebElement we_Timezone = parent.findElement(By.className(uiOutputTextArea));
                job.sked__Timezone__c = we_Timezone.getText();
            }
        }
        return job;

    }

    //define page element

    //By Description_label = By.className("slds-form-element_readonly");
    //slds-form-element slds-form-element_readonly slds-form-element_edit slds-grow slds-hint-parent override--slds-form-element
}
