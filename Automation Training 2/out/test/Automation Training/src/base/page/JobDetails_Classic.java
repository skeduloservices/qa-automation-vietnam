package base.page;

import base.utilities.Utilities;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import base.sked__object.sked__Job__c;
import sun.security.krb5.internal.crypto.Des;

public class JobDetails_Classic extends JobDetails_base {
    //protected WebDriver driver;
    //public SoftAssert softAssert;


    //init Login page
    public JobDetails_Classic(WebDriver driver, SoftAssert softAssert){
        super(driver, softAssert);
    }

    public sked__Job__c Get_Job_Info()
    {
        sked__Job__c job = new sked__Job__c();

        job.sked__Address__c = Utilities.waitForElement(driver, Address).getText();
        job.sked__Description__c = Utilities.waitForElement(driver, Description).getText();
        job.sked__Timezone__c = Utilities.waitForElement(driver, Timezone).getText();

        return job;
    }


    //define page element

    By Address = By.id("00N0b0000089glh_ileinner");
    By Description = By.id("00N0b0000089glj_ileinner");
    By Timezone = By.id("00N0b0000089gmp_ileinner");

}
