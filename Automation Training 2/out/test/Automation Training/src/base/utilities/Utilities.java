package base.utilities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.Properties;

public class Utilities {

    public static final int waitTime = 60;

    /**
     * This method is to retrieve data from a properties file and put it into a class
     * @param prop properties file
     * @param obj class to push data
     */
    public static void getTestData(Properties prop, Object obj){
        Enumeration propKeysList = prop.propertyNames();

        try {
            while (propKeysList.hasMoreElements()) {
                String propKey = propKeysList.nextElement().toString();
                for (Field f : obj.getClass().getFields()
                ) {
                    if (propKey.toLowerCase().equals(f.getName().toLowerCase())) {
                        Object value = prop.getProperty(propKey);
                        if (f.getType().equals(boolean.class)) {
                            if (value.toString().toLowerCase().equals("yes")) {
                                value = true;
                            } else {
                                value = false;
                            }
                        } else {
                            value = value.toString();
                            if(value.toString().isEmpty())
                                value = "";
                        }
                        f.set(obj, value);
                        break;
                    }
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * to get the folder path
     * @return
     */
    public static String getBasePath(){
        File test = new File("");
        return test.getAbsolutePath();
    }

    public static WebElement waitForElement(WebDriver driver, By elementLocator){
        WebDriverWait wait = new WebDriverWait(driver,waitTime);
        return wait.until(ExpectedConditions.elementToBeClickable(elementLocator));
    }

    public static String Get_Job_ID(String url){
        String tmp = url;
        String str_left, str_right;
        int left, right;
        str_left = "/sked__Job__c/";
        str_right = "/view";

        left = url.lastIndexOf(str_left);
        right = url.lastIndexOf(str_right);
        tmp = url.substring(left + str_left.length(), right);
        return tmp;
    }

}
