package base.datareaders;

import base.utilities.Utilities;

import java.util.Properties;

public class base_TestData {

    public String Device;

    public String StartDate;
    public String StartTime;
    public String Duration;
    public String Urgency;
    public String Description;
    public String CustomerJobNo;
    public String Region;
    public String AddressType;
    public String Site;
    public String Address;
    public String DeliveryMethod;
    public String Category;
    public String ServiceAgreementItem;

    //initiate test data
    //public base_TestData(Properties prop){
    //    Utilities.getTestData(prop,this);
    //}
}
